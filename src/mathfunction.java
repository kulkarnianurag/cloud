import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class mathfunction {

	
	public static Scanner sc=null;
	public static Scanner input=null;
	
	
	public static void main(String[] args) throws FileNotFoundException {
		
		
		//mathfunction mathobject = new mathfunction();
		
		//mathobject.run();
	}
	
	public void run(String inputname, String outputname) throws FileNotFoundException {
	
		int count=0;
		/*String filename = "C:\\Users\\Anurag\\Desktop\\sampleoutput1.txt";
		String outputfile = "C:\\Users\\Anurag\\Desktop\\mathoutputall.txt";*/
		
		String filename = inputname;
		String outputfile = outputname;
		
		PrintWriter out = new PrintWriter(outputfile);
		File file = new File(filename);
			
		System.out.println("finalinput: "+inputname+"  and finaloutput: "+outputname);
		
		String gasname[] ={"BRO","O3","NO2","HCHO","HONO","O4"};
		
		try{
		
		//	for(int gasindex=0;gasindex<6;gasindex++)
		//	{
		JSONArray set = new JSONArray();
		sc = new Scanner(file);
		input = new Scanner(file);
				
		while(input.hasNext())
		{
			JSONArray list = new JSONArray();
			
			JSONObject setobj = new JSONObject();
			JSONParser parser = new JSONParser();			
			String textline = input.nextLine();
					
			if (input.hasNext()==false)
			{
				break;
			}
			
	
			JSONObject newdata = new JSONObject();
			
			Object parseobj = parser.parse(textline);
			
			
			
			//System.out.println(textline);
			
			setobj = (JSONObject) parseobj;
			
								
		String MainDegreeValue = (String) setobj.get("MainDegree");
		boolean setdegreefound=false;
		
		list = (JSONArray)setobj.get("List");
		set = list;	
		//System.out.println(list);
		//System.exit(0);
		int j=0;
		for(j=0;j<set.size();j++)
		{					
			for(int gasindex=0;gasindex<6;gasindex++)
			{
				JSONObject coordinateset = new JSONObject();
				coordinateset=generatejsonobject(set,MainDegreeValue,gasname[gasindex]);
				//System.out.println(coordinateset);
				out.println(coordinateset);
				out.println();
				//coordinateset.clear();
							
			} // if angle ends
			set.clear();
		} // for j ends
		
			}//// for stringid ends
		
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
		
	}
		
		
	
	}// main ends

	
	public JSONObject generatejsonobject(JSONArray set, String MainDegreeValue, String gasname )
	{
		JSONObject coordinateset = new JSONObject();
		JSONObject temp = new JSONObject();
		JSONArray fitpoints = new JSONArray();
		JSONArray positivefitpoints = new JSONArray();
		JSONArray negativefitpoints = new JSONArray();
		
		temp = (JSONObject) set.get(0);
	    //System.out.println(temp);
		
		coordinateset.put("MainDegreeValue", MainDegreeValue);
		coordinateset.put("GasName", gasname);
		coordinateset.put("StartDate", temp.get("StartDate"));
		coordinateset.put("StartTime", temp.get("StartTime"));
		coordinateset.put("StopDate", temp.get("StopDate"));
		coordinateset.put("StopTime", temp.get("StopTime"));
		coordinateset.put("Xcoordinate", temp.get("SZA"));
		
		fitpoints = calculatefitpoints(set,MainDegreeValue, gasname);
		
		coordinateset.put("fitpointcoordinates", fitpoints);
		//System.out.println(coordinateset);
		
		String sign = "+";
		positivefitpoints = calculateerrorfitpoints(set, MainDegreeValue, gasname, sign);
		coordinateset.put("positivefitpointcoordinates", positivefitpoints);
		
		sign = "-";		
				
		negativefitpoints = calculateerrorfitpoints(set, MainDegreeValue, gasname, sign);
		coordinateset.put("negativefitpointcoordinates", negativefitpoints);
		
	//	System.out.println(coordinateset);
		//System.exit(0);
		return coordinateset;
	}
	
	
// -------------------generates fitpoints object	
public JSONArray calculatefitpoints(JSONArray set, String MainDegree, String gasname)
{
	JSONArray fitpointobject = new JSONArray();
	
	JSONObject temp = new JSONObject();
	
	String fitgasname = gasname + "Fit"; 
	
	temp = (JSONObject) set.get(0);
	
	String fitgaspoint = (String) temp.get(fitgasname);
	double maindegreefitpoint = Double.parseDouble(fitgaspoint);
	//BigDecimal maindegreefitpoint = BigDecimal.
	
	System.out.println();
	fitpointobject.clear();
	
	int arrayindex=0;
	
	for(int i=1;i<set.size();i++)
	{
		JSONObject ycoordinate = new JSONObject();
		temp = new JSONObject();
		temp = (JSONObject) set.get(i);
		String subdegree="";
		String subfitpoint="";
		
		subdegree = (String) temp.get("ReferenceAngle");
	
		subfitpoint = (String) temp.get(fitgasname);
		
		Double subdegreefitpoint = 0.0;
		subdegreefitpoint = Double.parseDouble(subfitpoint);
				
		Double difference = 0.0;
		difference = subdegreefitpoint - maindegreefitpoint;
				
		String differencevalue = "";
		differencevalue = Double.toString(difference);
	
		ycoordinate.put("SubDegree", subdegree);
		ycoordinate.put("Ycoordinate", differencevalue);		
		fitpointobject.add(arrayindex, ycoordinate);
		arrayindex++;
		//System.exit(0);
	}
	
	return fitpointobject;
}
	

//-------------generates positive and negative fit points object
	
public JSONArray calculateerrorfitpoints(JSONArray set, String MainDegree, String gasname, String sign)
{
	JSONArray errorfitpoints = new JSONArray();
	JSONObject temp = new JSONObject();
	JSONObject fittemp = new JSONObject();
	
	String fitgasnameerror = gasname + "Error"; 
	String fitgasname = gasname + "Fit";
	temp = (JSONObject) set.get(0);
	
	String fitgaspoint = (String) temp.get(fitgasname);
	double maindegreefitpoint = Double.parseDouble(fitgaspoint);
	
	String fitgaserrorpoint = (String) temp.get(fitgasnameerror);
	double maindegreeerrorfitpoint = Double.parseDouble(fitgaserrorpoint);
	
	String subdegree = "";
	for(int i=1; i<set.size();i++)
	{
		JSONObject ycoordinate = new JSONObject();
		
		temp = new JSONObject();
		temp = (JSONObject) set.get(i);
		
		//gives fit value (1)
		String subfitgaspoint = "";
		subfitgaspoint =(String) temp.get(fitgasname);
		double subdegreefitpoint = 0;
		subdegreefitpoint = Double.parseDouble(subfitgaspoint);
		
		//gives error fitvalue (2)
		String subfitgaserrorpoint = "";
		subfitgaserrorpoint = (String) temp.get(fitgasnameerror);
		
		double subdegreeerrorfitpoint = 0;
		subdegreeerrorfitpoint = Double.parseDouble(subfitgaserrorpoint);
		
		double difference = 0;
		difference = subdegreefitpoint - maindegreefitpoint;
				
		double yco = 0;
		yco = maindegreeerrorfitpoint + subdegreeerrorfitpoint ;
				
		if(sign.equalsIgnoreCase("+"))
		{
		 yco = difference + yco;  	//gives y cordinate 
		}
		
		else
		{
		 yco = difference - yco;			// gives y coordinate
		}
		
		String ycoordinatevalue = "";
		ycoordinatevalue = Double.toString(yco);
		subdegree = (String) temp.get("ReferenceAngle");		
		ycoordinate.put("SubDegree", subdegree);
		ycoordinate.put("Ycoordinate", ycoordinatevalue);
		errorfitpoints.add(ycoordinate);
	}
	
//	System.out.println(errorfitpoints);
	
//	System.exit(0);
	return errorfitpoints;
	
}
	
	
}//class ends
