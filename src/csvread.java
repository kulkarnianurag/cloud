import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;




public class csvread {

	
	public static Scanner sc=null;
	public static Scanner sc1=null;
	
	public static void main(String[] args) throws FileNotFoundException, ParseException {
	
		csvread obj = new csvread();
		obj.run(args[0],args[1], args[2]);
	}
	
	private void run(String inputname, String outputname, String finaloutputname) throws FileNotFoundException, ParseException {
		// TODO Auto-generated method stub
		
		
		
		/*String csvFile = "\\home\\ubuntu\\spectra.csv";
		String outputfile = "\\home\\ubuntu\\dataoutputa\\sampleoutput.txt";
		String outputfile = "\\home\\ubuntu\\dataoutputb\\sampleoutput.txt";*/
		
		String csvFile = inputname;
		
		/*String csvFile = "C:\\Users\\Anurag\\Desktop\\spectra.csv";
		String outputfile = "C:\\Users\\Anurag\\Desktop\\sampleoutput.txt";*/
			
		
		//git clone https://kulkarnianurag@bitbucket.org/kulkarnianurag/cloud.git
		
		try {
		
		for(int indexfile=0; indexfile <100;indexfile++){
		System.out.println("Index is: "+indexfile);
		String newoutputname = outputname + Integer.toString(indexfile)+".txt";
		
		String outputfile = newoutputname;
		
		PrintWriter out = new PrintWriter(outputfile);
		File file = new File(csvFile);
		BufferedReader br = null;
		BufferedReader newbr = null;
		String line = "";
		String cvsSplitBy = ",";
		int i=0;	 
		
		sc = new Scanner(file);
		
					
			br = new BufferedReader(new FileReader(csvFile));
			newbr = new BufferedReader(new FileReader(csvFile));
			String newline = "";
			
			boolean setmark = false;
			
			while (sc.hasNext()) {
	 
				 // use comma as separator
				
				if(setmark == false)
				{
				line = sc.nextLine();
				}
				
				else
				{
					line = newline;					
				}
				
				setmark = false;
				
				String[] spectra = line.split(cvsSplitBy);
				
				if(i>0 && !spectra[153].equalsIgnoreCase("dc") )
		    	{
		    	
		    	if(spectra[153].equalsIgnoreCase("90"))	
		    	{
		    	
		    	 JSONArray list = new JSONArray();	
		    	
		    	 JSONObject set = new JSONObject();
		    	 		    	 	
		    	 String startdate=spectra[145];
		    	 String stopdate=spectra[148];
		    	 
		    	 if(startdate.contains("/"))
		    	 {
		    		 	String startdate1=startdate;
				 	    DateFormat df=new SimpleDateFormat("dd/mm/yyyy");
				 	    Date d= df.parse(startdate1);
				 	    df=new SimpleDateFormat("dd-mm-yyyy");
				 	    startdate = df.format(d);
				 	   
				 	    String stopdate1 = stopdate;
				 	   df=new SimpleDateFormat("dd/mm/yyyy");
				 	    d= df.parse(stopdate1);
				 	    	
				 	   df=new SimpleDateFormat("dd-mm-yyyy");
				 	  stopdate = df.format(d);
				 	    
		    		 //System.out.println("startdate is: "+startdate+"   stopdate is: "+ stopdate);
		    		 
		    		 
		    	 }
		    	 
		    	
		    	 
		    	 JSONObject obj = new JSONObject();
				 obj.put("StartDate",startdate);
				 obj.put("StartTime",spectra[147]);
				 obj.put("StopDate",stopdate);
				 obj.put("StopTime",spectra[150]);
				 obj.put("Latitude",spectra[121]);
				 obj.put("Longitude",spectra[124]);
				 obj.put("TimeZoneOffset",spectra[152]);
				 obj.put("ReferenceAngle",spectra[153]);
				 obj.put("SZA",spectra[154]);
				 obj.put("O3Fit", spectra[30]);
				 obj.put("O3Error", spectra[31]);
				 obj.put("NO2Fit", spectra[40]);
				 obj.put("NO2Error", spectra[41]);
				 obj.put("BROFit", spectra[50]);
				 obj.put("BROError", spectra[51]);
				 obj.put("HCHOFit", spectra[60]);
				 obj.put("HCHOError", spectra[61]);
				 obj.put("O4Fit", spectra[70]);
				 obj.put("O4Error", spectra[71]);
				 obj.put("HONOFit", spectra[80]);
				 obj.put("HONOError", spectra[81]);
				
			
				 set.put("MainDegree",spectra[153]);
				 				
				
				 list.add(obj); //1st 90 object row is added
				 
				 i++;
				 
				
				 
				 boolean setdegreefound=false;
				 
				
				 while(setdegreefound == false && sc.hasNext() )
				 {
					 
					 newline = sc.nextLine();
					 String[] subspectra = newline.split(cvsSplitBy);
					
					
						 if(!subspectra[153].equalsIgnoreCase("dc"))
						 {
							 
							 if(subspectra[153].equalsIgnoreCase("90"))
							 
							 {
								
								 setdegreefound = true;
								 setmark = true;
								 line = newline;
							 }
							 
							 else
							 {
								 JSONObject obj1 = new JSONObject();
								 
								  startdate=subspectra[145];
						    	  stopdate=subspectra[148];
						    	 
						    	 if(startdate.contains("/"))
						    	 {
						    		 	String startdate1=startdate;
								 	    DateFormat df=new SimpleDateFormat("dd/mm/yyyy");
								 	    Date d= df.parse(startdate1);
								 	    df=new SimpleDateFormat("dd-mm-yyyy");
								 	    startdate = df.format(d);
								 	   
								 	    String stopdate1 = stopdate;
								 	   df=new SimpleDateFormat("dd/mm/yyyy");
								 	    d= df.parse(stopdate1);
								 	    	
								 	   df=new SimpleDateFormat("dd-mm-yyyy");
								 	  stopdate = df.format(d);
								 	    
						    		 //System.out.println("startdate is: "+startdate+"   stopdate is: "+ stopdate);
						    		 
						    		 
						    	 }
								 obj1.put("StartDate",startdate);
								 obj1.put("StartTime",subspectra[147]);
								 obj1.put("StopDate",stopdate);
								 obj1.put("StopTime",subspectra[150]);
								 obj1.put("Latitude",subspectra[121]);
								 obj1.put("Longitude",subspectra[124]);
								 obj1.put("TimeZoneOffset",subspectra[152]);
								 obj1.put("ReferenceAngle",subspectra[153]);
								 obj1.put("SZA",subspectra[154]);
								 obj1.put("O3Fit", subspectra[30]);
								 obj1.put("O3Error", subspectra[31]);
								 obj1.put("NO2Fit", subspectra[40]);
								 obj1.put("NO2Error", subspectra[41]);
								 obj1.put("BROFit", subspectra[50]);
								 obj1.put("BROError", subspectra[51]);
								 obj1.put("HCHOFit", subspectra[60]);
								 obj1.put("HCHOError", subspectra[61]);
								 obj1.put("O4Fit", subspectra[70]);
								 obj1.put("O4Error", subspectra[71]);
								 obj1.put("HONOFit", subspectra[80]);
								 obj1.put("HONOError", subspectra[81]);
								
								 
								 
							
								 list.add(obj1);
								 i++;
								 
								 
							 } // if spectra is not 90 put it into the list
							 
							 
						 }// checki if dc is present or 
						 
				 
				 } // while loop that adds other sub values to a set
				 
				 set.put("List",list);
				 
				out.println(set.toString());
				// System.out.println(set);  /// here the set is outputted
								 
		    	}//if set = 90 ends
				 
		    	} //if loop ends
		    	
			//	br.mark(i);
			//	br.reset();
		    	i++;
		    	
		    	}// while ends
			
			String newfinaloutputname = finaloutputname + Integer.toString(indexfile)+".txt";
			System.out.println("end index is: "+indexfile);
			System.out.println("start file: "+outputfile+"   and end file is: "+newfinaloutputname);			

			mathfunction mathobject = new mathfunction();
			

			mathobject.run(outputfile,newfinaloutputname);

			System.out.println("end index is: "+indexfile);
			
		}			
			
	
			
			
		}// try ends
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Done");
		}
	 
		

} // run ends

	
	
	
}// class ends	
