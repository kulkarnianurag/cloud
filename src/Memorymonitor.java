import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



public class Memorymonitor {
	
	
	  static Socket s1 = null;
	  
  public static String printUsage(Runtime runtime, String IP) {
    long total, free, used;

    total = runtime.totalMemory();
    free  = runtime.freeMemory();
    used = total - free;

    
    
    System.out.println("\nTotal Memory: " + total);
    System.out.println("        Used: " + used);
    System.out.println("        Free: " + free);
    double percentu = (((double)used/(double)total)*100);;
    String percentused = Double.toString(percentu);
    
    double percenrf = (((double)free/(double)total)*100);;
    String percentfree = Double.toString(percenrf);
    
    System.out.println("Percent Used: " + ((double)used/(double)total)*100);
    System.out.println("Percent Free: " + ((double)free/(double)total)*100);
    
    String object = Long.toString(total) + " " + Long.toString(used) + " " + Long.toString(free) + " " + percentused + " " + percentfree;
    
    String status = "";
    
    status = createvmstatusobject(object, IP);
    
    return status;
    
  }

 	public static void main(String args[]) throws IOException
		{
					
 		DataOutputStream os = null;
			DataInputStream is = null;
			try {
				ServerSocket server = new ServerSocket(3000);
				while (true ) {
					Runtime runtime;
						
					
						s1 = server.accept();
						is = new DataInputStream(s1.getInputStream());
						os = new DataOutputStream(s1.getOutputStream());
						String word=is.readUTF();
						
						String sentence[] = word.split(" ");
						
						System.out.println("word is "+word);
						
						String newword = sentence[0];
						String IP = sentence[1];
						
						
						if(newword.equalsIgnoreCase("status"))
						{
						
					    byte[] bytes;

					   
					    runtime = Runtime.getRuntime();
					
				   String output = printUsage(runtime, IP); 
				   
				   os.writeUTF(output);
						}
						
						
  }
}

		     catch(Exception e) {
		    	 os.writeUTF("error");
		    	 e.printStackTrace();
		        return;
		      }
		}
 	
 	
 	public static String createvmstatusobject(String json, String IP)
 	{
 		String finalobject[] = null;
 		String vmobjects="";
 		
 		try {
 		finalobject = json.split(" ");
 		//JSONArray vmarray = new JSONArray();

 		JSONObject object = new JSONObject();
 		
 		for(int i=0 ;i<1;i++)
 		{
 			
 			 object.put("total",finalobject[i]);
 			 object.put("used",finalobject[i+1]);
 			 object.put("free",finalobject[i+2]);
 			 object.put("percentused",finalobject[i+3]);
 			 object.put("percentfree",finalobject[i+4]);
 			// vmarray.add(object);
 			 
 		}

 		
 		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
 		Date date = new Date();
 		String datevalue = dateFormat.format(date);
 		
 		
 		
 		object.put("ipaddress", IP);
 		object.put("date", datevalue);
 		
 		
 		 vmobjects = object.toString();
 		
 		}
 		
 		catch(Exception e)
 		{
 			e.printStackTrace();
 		}
 		
 		return vmobjects;
 	}

 	
 	
}
